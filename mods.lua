
-- craft bones from animalmaterials into bonemeal
if minetest.get_modpath("animalmaterials") then

	minetest.register_craft({
		output = "bonemeal:bonemeal 2",
		recipe = {{"animalmaterials:bone"}}
	})
end


if minetest.get_modpath("default") then

	-- saplings

	local function pine_grow(pos)

		if minetest.find_node_near(pos, 1,
			{"default:snow", "default:snowblock", "default:dirt_with_snow"}) then

			default.grow_new_snowy_pine_tree(pos)
		else
			default.grow_new_pine_tree(pos)
		end
	end

	local function cactus_grow(pos)
		default.grow_cactus(pos, minetest.get_node(pos))
	end

	local function papyrus_grow(pos)
		default.grow_papyrus(pos, minetest.get_node(pos))
	end

	bonemeal:add_sapling({
		{"default:sapling", default.grow_new_apple_tree, "soil"},
		{"default:junglesapling", default.grow_new_jungle_tree, "soil"},
		{"default:emergent_jungle_sapling", default.grow_new_emergent_jungle_tree, "soil"},
		{"default:acacia_sapling", default.grow_new_acacia_tree, "soil"},
		{"default:aspen_sapling", default.grow_new_aspen_tree, "soil"},
		{"default:pine_sapling", pine_grow, "soil"},
		{"default:bush_sapling", default.grow_bush, "soil"},
		{"default:acacia_bush_sapling", default.grow_acacia_bush, "soil"},
		{"default:large_cactus_seedling", default.grow_large_cactus, "sand"},
		{"default:blueberry_bush_sapling", default.grow_blueberry_bush, "soil"},
		{"default:pine_bush_sapling", default.grow_pine_bush, "soil"},
		{"default:cactus", cactus_grow, "sand"},
		{"default:papyrus", papyrus_grow, "soil"}
	})

	-- decoration

	local green_grass = {
		"default:grass_2", "default:grass_3", "default:grass_4",
		"default:grass_5", "", ""
	}

	local dry_grass = {
		"default:dry_grass_2", "default:dry_grass_3", "default:dry_grass_4",
		"default:dry_grass_5", "", ""
	}

	local flowers = {}

	minetest.after(0.1, function()

		for node, def in pairs(minetest.registered_nodes) do

			if def.groups
			and def.groups.flower
			and not node:find("waterlily")
			and not node:find("seaweed")
			and not node:find("xdecor:potted_")
			and not node:find("df_farming:") then
				flowers[#flowers + 1] = node
			end
		end
	end)

	bonemeal:add_deco({
		{"default:dirt", bonemeal.green_grass, flowers},
		{"default:dirt_with_grass", green_grass, flowers},
		{"default:dry_dirt", dry_grass, {}},
		{"default:dry_dirt_with_dry_grass", dry_grass, {}},
		{"default:dirt_with_dry_grass", dry_grass, flowers},
		{"default:sand", {}, {"default:dry_shrub", "", "", ""} },
		{"default:desert_sand", {}, {"default:dry_shrub", "", "", ""} },
		{"default:silver_sand", {}, {"default:dry_shrub", "", "", ""} },
		{"default:dirt_with_rainforest_litter", {}, {"default:junglegrass", "", "", ""}}
	})
end


if farming then

	bonemeal:add_crop({
		{"farming:cotton_", 8, "farming:seed_cotton"},
		{"farming:wheat_", 8, "farming:seed_wheat"}
	})
end


if farming and farming.mod and farming.mod == "redo" then

	bonemeal:add_crop({
		{"farming:tomato_", 8},
		{"farming:corn_", 8},
		{"farming:melon_", 8},
		{"farming:pumpkin_", 8},
		{"farming:beanpole_", 5},
		{"farming:blueberry_", 4},
		{"farming:raspberry_", 4},
		{"farming:carrot_", 8},
		{"farming:cocoa_", 4},
		{"farming:coffee_", 5},
		{"farming:cucumber_", 4},
		{"farming:potato_", 4},
		{"farming:grapes_", 8},
		{"farming:rhubarb_", 4},
		{"farming:barley_", 8, "farming:seed_barley"},
		{"farming:hemp_", 8, "farming:seed_hemp"},
		{"farming:chili_", 8},
		{"farming:garlic_", 5},
		{"farming:onion_", 5},
		{"farming:pepper_", 7},
		{"farming:pineapple_", 8},
		{"farming:pea_", 5},
		{"farming:beetroot_", 5},
		{"farming:rye_", 8, "farming:seed_rye"},
		{"farming:oat_", 8, "farming:seed_oat"},
		{"farming:rice_", 8, "farming:seed_rice"},
		{"farming:mint_", 4, "farming:seed_mint"},
		{"farming:cabbage_", 6},
		{"farming:lettuce_", 5},
		{"farming:blackberry_", 4},
		{"farming:vanilla_", 8},
		{"farming:soy_", 7},
		{"farming:artichoke_", 5},
		{"farming:parsley_", 3},
		{"farming:sunflower_", 8, "farming:seed_sunflower"},
		{"farming:asparagus_", 5},
		{"farming:eggplant_", 4},
		{"farming:spinach_", 4},
		{"farming:ginger_", 4},
		{"ethereal:strawberry_", 8}
	})
end


if minetest.get_modpath("ethereal") then

	bonemeal:add_crop({
		{"ethereal:strawberry_", 8},
		{"ethereal:onion_", 5}
	})

	bonemeal:add_sapling({
		{"ethereal:palm_sapling", ethereal.grow_palm_tree, "soil"},
		{"ethereal:palm_sapling", ethereal.grow_palm_tree, "sand"},
		{"ethereal:yellow_tree_sapling", ethereal.grow_yellow_tree, "soil"},
		{"ethereal:big_tree_sapling", ethereal.grow_big_tree, "soil"},
		{"ethereal:banana_tree_sapling", ethereal.grow_banana_tree, "soil"},
		{"ethereal:frost_tree_sapling", ethereal.grow_frost_tree, "soil"},
		{"ethereal:mushroom_sapling", ethereal.grow_mushroom_tree, "soil"},
		{"ethereal:mushroom_brown_sapling", ethereal.grow_mushroom_brown_tree, "soil"},
		{"ethereal:willow_sapling", ethereal.grow_willow_tree, "soil"},
		{"ethereal:redwood_sapling", ethereal.grow_redwood_tree, "soil"},
		{"ethereal:giant_redwood_sapling", ethereal.grow_giant_redwood_tree, "soil"},
		{"ethereal:orange_tree_sapling", ethereal.grow_orange_tree, "soil"},
		{"ethereal:bamboo_sprout", ethereal.grow_bamboo_tree, "soil"},
		{"ethereal:birch_sapling", ethereal.grow_birch_tree, "soil"},
		{"ethereal:sakura_sapling", ethereal.grow_sakura_tree, "soil"},
		{"ethereal:lemon_tree_sapling", ethereal.grow_lemon_tree, "soil"},
		{"ethereal:olive_tree_sapling", ethereal.grow_olive_tree, "soil"},
		{"ethereal:basandra_bush_sapling", ethereal.grow_basandra_bush, "soil"}
	})

	local grass = {"default:grass_3", "default:grass_4", "default:grass_5", ""}

	bonemeal:add_deco({
		{"ethereal:crystal_dirt", {"ethereal:crystalgrass", "", "", "", ""}, {}},
		{"ethereal:fiery_dirt", {"ethereal:dry_shrub", "", "", "", ""}, {}},
		{"ethereal:prairie_dirt", grass, {"flowers:dandelion_white",
			"flowers:dandelion_yellow", "flowers:geranium", "flowers:rose",
			"flowers:tulip", "flowers:viola", "ethereal:strawberry_7"}},
		{"ethereal:gray_dirt", {}, {"ethereal:snowygrass", "", ""}},
		{"ethereal:cold_dirt", {}, {"ethereal:snowygrass", "", ""}},
		{"ethereal:mushroom_dirt", {}, {"flowers:mushroom_red", "flowers:mushroom_brown",
				"ethereal:spore_grass", "ethereal:spore_grass", "", "", ""}},
		{"ethereal:jungle_dirt", grass, {"default:junglegrass", "", "", ""}},
		{"ethereal:grove_dirt", grass, {"ethereal:fern", "", "", ""}},
		{"ethereal:bamboo_dirt", grass, {}}
	})
end


if minetest.get_modpath("moretrees") then

	-- special fir check for snow
	local function fir_grow(pos)

		if minetest.find_node_near(pos, 1,
			{"default:snow", "default:snowblock", "default:dirt_with_snow"}) then

			moretrees.grow_fir_snow(pos)
		else
			moretrees.grow_fir(pos)
		end
	end

	bonemeal:add_sapling({
		{"moretrees:beech_sapling", moretrees.spawn_beech_object, "soil"},
		{"moretrees:apple_tree_sapling", moretrees.spawn_apple_tree_object, "soil"},
		{"moretrees:oak_sapling", moretrees.spawn_oak_object, "soil"},
		{"moretrees:sequoia_sapling", moretrees.spawn_sequoia_object, "soil"},
		{"moretrees:birch_sapling", moretrees.grow_birch, "soil"},
		{"moretrees:palm_sapling", moretrees.spawn_palm_object, "soil"},
		{"moretrees:palm_sapling", moretrees.spawn_palm_object, "sand"},
		{"moretrees:date_palm_sapling", moretrees.spawn_date_palm_object, "soil"},
		{"moretrees:date_palm_sapling", moretrees.spawn_date_palm_object, "sand"},
		{"moretrees:spruce_sapling", moretrees.grow_spruce, "soil"},
		{"moretrees:cedar_sapling", moretrees.spawn_cedar_object, "soil"},
		{"moretrees:poplar_sapling", moretrees.spawn_poplar_object, "soil"},
		{"moretrees:poplar_small_sapling", moretrees.spawn_poplar_small_object, "soil"},
		{"moretrees:willow_sapling", moretrees.spawn_willow_object, "soil"},
		{"moretrees:rubber_tree_sapling", moretrees.spawn_rubber_tree_object, "soil"},
		{"moretrees:fir_sapling", fir_grow, "soil"}
	})

elseif minetest.get_modpath("technic_worldgen") then

	bonemeal:add_sapling({
		{"moretrees:rubber_tree_sapling", technic.rubber_tree_model, "soil"}
	})
end


if minetest.get_modpath("caverealms") then

	local fil = minetest.get_modpath("caverealms") .. "/schematics/shroom.mts"
	local add_shroom = function(pos)

		minetest.swap_node(pos, {name = "air"})

		minetest.place_schematic(
			{x = pos.x - 5, y = pos.y, z = pos.z - 5}, fil, 0, nil, false)
	end

	bonemeal:add_sapling({
		{"caverealms:mushroom_sapling", add_shroom, "soil"}
	})
end


local function y_func(grow_func)
	return function(pos)
		grow_func({x = pos.x, y = pos.y - 1, z = pos.z})
	end
end

if minetest.get_modpath("ferns") then

	bonemeal:add_sapling({
		{"ferns:sapling_giant_tree_fern", y_func(abstract_ferns.grow_giant_tree_fern), "soil"},
		{"ferns:sapling_giant_tree_fern", y_func(abstract_ferns.grow_giant_tree_fern), "sand"},
		{"ferns:sapling_tree_fern", y_func(abstract_ferns.grow_tree_fern), "soil"}
	})
end

if minetest.get_modpath("dryplants") then

	bonemeal:add_sapling({
		{"dryplants:reedmace_sapling", y_func(abstract_dryplants.grow_reedmace), "soil"}
	})
end


if minetest.get_modpath("dye") then

	local bonemeal_dyes = {bonemeal = "white", fertiliser = "green", mulch = "brown"}

	for mat, dye in pairs(bonemeal_dyes) do

		minetest.register_craft({
			output = "dye:" .. dye .. " 4",
			recipe = {
				{"bonemeal:" .. mat}
			},
		})
	end
end


if minetest.get_modpath("df_trees") then

	local function spore_tree_fix(pos)
		minetest.set_node(pos, {name = "air"})
		df_trees.spawn_spore_tree(pos)
	end

	local function fungiwood_fix(pos)
		minetest.set_node(pos, {name = "air"})
		df_trees.spawn_fungiwood(pos)
	end

	local function tunnel_fix(pos)
		minetest.set_node(pos, {name = "air"})
		df_trees.spawn_tunnel_tube(pos)
	end

	bonemeal:add_sapling({
		{"df_trees:black_cap_sapling", df_trees.spawn_black_cap, "soil", true},
		{"df_trees:fungiwood_sapling", fungiwood_fix, "soil", true},
		{"df_trees:goblin_cap_sapling", df_trees.spawn_goblin_cap, "soil", true},
		{"df_trees:spore_tree_sapling", spore_tree_fix, "soil", true},
		{"df_trees:tower_cap_sapling", df_trees.spawn_tower_cap, "soil", true},
		{"df_trees:tunnel_tube_sapling", tunnel_fix, "soil", true},
		{"df_trees:nether_cap_sapling", df_trees.spawn_nether_cap, "group:nether_cap", true},
		{"df_trees:nether_cap_sapling", df_trees.spawn_nether_cap, "group:cools_lava", true}
	})
end


if minetest.get_modpath("df_farming") then

	bonemeal:add_crop({
		{"df_farming:cave_wheat_", 8, "df_farming:cave_wheat_seed", true},
		{"df_farming:dimple_cup_", 4, "df_farming:dimple_cup_seed", true},
		{"df_farming:pig_tail_", 8, "df_farming:pig_tail_seed", true},
		{"df_farming:plump_helmet_", 4, "df_farming:plump_helmet_spawn", true},
		{"df_farming:quarry_bush_", 5, "df_farming:quarry_bush_seed", true},
		{"df_farming:sweet_pod_", 6, "df_farming:sweet_pod_seed", true}
	})
end


if minetest.get_modpath("df_primordial_items") then

	local function mush_fix(pos)
		minetest.set_node(pos, {name = "air"})
		mapgen_helper.place_schematic(pos,
			df_primordial_items.get_primordial_mushroom(), (math.random(4) - 1) * 90)
	end

	local function fern_fix(pos)
		minetest.set_node(pos, {name = "air"})
		local rotations = {0, 90, 180, 270}
		mapgen_helper.place_schematic(pos,
			df_primordial_items.get_fern_schematic(), rotations[math.random(#rotations)])
	end

	local function blood_fix(pos)
		df_trees.grow_blood_thorn(pos, minetest.get_node(pos))
	end

	bonemeal:add_sapling({
		{"df_primordial_items:jungle_mushroom_sapling",
				df_primordial_items.spawn_jungle_mushroom, "soil", true},
		{"df_primordial_items:jungletree_sapling",
				df_primordial_items.spawn_jungle_tree, "soil", true},
		{"df_primordial_items:mush_sapling", mush_fix, "soil", true},
		{"df_primordial_items:fern_sapling", fern_fix, "soil", true},
		{"df_trees:blood_thorn", blood_fix, "sand", true}
	})

	local jgrass = {
		"df_primordial_items:jungle_grass_1",
		"df_primordial_items:jungle_grass_2",
		"df_primordial_items:jungle_grass_3",
		"df_primordial_items:fern_1",
		"df_primordial_items:fern_2",
		"", "", "", ""
	}

	local jdeco = {
		"df_primordial_items:jungle_mushroom_1",
		"df_primordial_items:jungle_mushroom_2",
		"df_primordial_items:glow_plant_1",
		"df_primordial_items:glow_plant_2",
		"df_primordial_items:glow_plant_3",
		"", "", ""
	}

	bonemeal:add_deco({
		{"df_primordial_items:dirt_with_jungle_grass", jgrass, jdeco}
	})

	local fgrass = {
		"df_primordial_items:fungal_grass_1",
		"df_primordial_items:fungal_grass_2",
		"", "", "", ""
	}

	local fdeco = {
		"df_primordial_items:glow_orb_stalks",
		"df_primordial_items:glow_pods",
		"", "", ""
	}

	bonemeal:add_deco({
		{"df_primordial_items:dirt_with_mycelium", fgrass, fdeco}
	})
end


if minetest.get_modpath("everness") then

	bonemeal:add_sapling({
		{"everness:baobab_sapling", Everness.grow_baobab_tree, "soil"},
		{"everness:coral_tree_bioluminescent_sapling",
				Everness.coral_tree_bioluminescent, "soil"},
		{"everness:coral_tree_sapling", Everness.grow_coral_tree, "soil"},
		{"everness:crystal_bush_sapling", Everness.grow_crystal_bush, "soil"},
		{"everness:crystal_tree_large_sapling", Everness.grow_crystal_large_tree, "soil"},
		{"everness:crystal_tree_sapling", Everness.grow_crystal_tree, "soil"},
		{"everness:cursed_bush_sapling", Everness.grow_cursed_bush, "soil"},
		{"everness:cursed_dream_tree_sapling", Everness.grow_cursed_dream_tree, "soil"},
		{"everness:dry_tree_sapling", Everness.grow_dry_tree, "soil"},
		{"everness:sequoia_tree_sapling", Everness.grow_sequoia_tree, "soil"},
		{"everness:willow_tree_sapling", Everness.grow_willow_tree, "soil"},
		{"everness:mese_tree_sapling", Everness.grow_mese_tree, "soil"},
		{"everness:dry_tree_sapling", Everness.grow_dry_tree, "soil"},
		{"everness:palm_tree_sapling", Everness.grow_palm_tree, "soil"},
		{"everness:lava_tree_sapling", Everness.grow_lava_tree, "soil"},
	})

	bonemeal:add_deco({
		{"everness:dry_dirt", {"everness:dry_grass_1", "everness:dry_grass_2", "everness:dry_grass_3", "everness:dry_grass_4", ""}, {}},
		{"everness:dry_dirt_with_dry_grass", {"everness:dry_grass_1", "everness:dry_grass_2", "everness:dry_grass_3", "everness:dry_grass_4", ""}, {}},
		{"everness:coral_dirt", {"everness:coral_grass_1", "everness:coral_grass_2", "everness:coral_grass_3", "everness:coral_grass_4", "everness:coral_grass_5"}, {"everness:coral_burdock_1", "everness:coral_burdock_2"}},
		{"everness:dirt_with_coral_grass", {"everness:coral_grass_1", "everness:coral_grass_2", "everness:coral_grass_3", "everness:coral_grass_4", "everness:coral_grass_5"}, {"everness:coral_burdock_1", "everness:coral_burdock_2"}},
		{"everness:crystal_dirt", {"everness:crystal_grass_1", "everness:crystal_grass_2", "everness:crystal_grass_3", "", ""}, {"everness:crystal_mushrooms"}},
		{"everness:dirt_with_crystal_grass", {"everness:crystal_grass_1", "everness:crystal_grass_2", "everness:crystal_grass_3", "", ""}, {"everness:crystal_mushrooms"}},
		-- nothing for crystal cave dirt/grass
		{"everness:cursed_dirt", {"everness:red_castor_1", "everness:red_castor_2", "everness:red_castor_3", "everness:red_castor_4", ""}, {}},
		{"everness:dirt_with_cursed_grass", {"everness:red_castor_1", "everness:red_castor_2", "everness:red_castor_3", "everness:red_castor_4", ""}, {}},
		{"everness:forsaken_tundra_dirt", {"everness:bloodspore_plant", "", "", "", ""}, {}},
		{"everness:forsaken_tundra_dirt_with_grass", {"everness:bloodspore_plant", "", "", "", ""}, {}},
		-- not doing anything for everness dirt_1, dirt_with_grass, etc.
	})

	minetest.register_craft({
		output = 'bonemeal:bonemeal 4',
		type = "shapeless",
		recipe = {
			'everness:bone',
		},
	})
end


if minetest.get_modpath("bushes_classic") then

	local function grow_bush(pos)

		local meta = minetest.get_meta(pos)
		local bush_name = meta:get_string("bush_type")

		-- only change if meta found
		if meta and bush_name then
			minetest.swap_node(pos, {name = "bushes:" .. bush_name .. "_bush"})
		end
	end

	bonemeal:add_sapling({
		{"bushes:fruitless_bush", grow_bush, "soil"},
	})
end


if minetest.get_modpath("x_farming") then

	bonemeal:add_sapling({
		{"x_farming:christmas_tree_sapling", x_farming.grow_christmas_tree, "soil"},
		{"x_farming:kiwi_sapling", x_farming.grow_kiwi_tree, "soil"},
		{"x_farming:large_cactus_with_fruit_seedling", x_farming.grow_large_cactus, "sand"},
		{"x_farming:jungle_with_cocoa_sapling", x_farming.grow_jungle_tree, "soil"},
		{"x_farming:pine_nut_sapling", x_farming.grow_pine_nut_tree, "soil"},
	})

	-- TODO: add all x_farming crops, not really needed on VAL
	-- but for anyone else who wants to use our mods
	bonemeal:add_crop({
		{"x_farming:stevia_", 8, "x_farming:seed_stevia"},
		{"x_farming:obsidian_wart_", 6, "x_farming:seed_obsidian_wart"},
	})
end


if minetest.get_modpath("cherry_biome") then

	bonemeal:add_sapling({
		{"cherry_biome:sakura_sapling", cherry_biome.grow_sakura_sapling, "soil"},
	})

	bonemeal:add_deco({
		{"cherry_biome:dirt_with_cherry_grass", {"default:grass_1", "default:grass_2", "default:grass_3", "default:grass_4", "default:grass_5"},
			{""} }
	})
end


if minetest.get_modpath("ebiomes") then

	bonemeal:add_sapling({
		{"ebiomes:beech_sapling", ebiomes.grow_new_beech_tree, "soil"},
		{"ebiomes:bush_sapling_warm", ebiomes.grow_new_bush_warm, "soil"},
		{"ebiomes:bush_sapling_warm", ebiomes.grow_new_bush_warm, "soil"},
		{"ebiomes:cypress_sapling", ebiomes.grow_new_cypress, "soil"},
		{"ebiomes:olive_sapling", ebiomes.grow_new_olive, "soil"},
		{"ebiomes:blackcurrant_bush_sapling", ebiomes.grow_new_blackcurrant, "soil"},
		{"ebiomes:redcurrant_bush_sapling", ebiomes.grow_new_redcurrant, "soil"},
		{"ebiomes:gooseberry_bush_sapling", ebiomes.grow_new_gooseberry, "soil"},
		{"ebiomes:willow_sapling", ebiomes.grow_new_willow_tree, "soil"},
		{"ebiomes:alder_sapling", ebiomes.grow_new_alder_tree, "soil"},
		{"ebiomes:ash_sapling", ebiomes.grow_new_ash_tree, "soil"},
		{"ebiomes:birch_sapling", ebiomes.grow_new_birch_tree, "soil"},
		{"ebiomes:downy_birch_sapling", ebiomes.grow_new_downy_birch_tree, "soil"},
		{"ebiomes:pear_sapling", ebiomes.grow_new_pear_tree, "soil"},
		{"ebiomes:quince_sapling", ebiomes.grow_new_quince_tree, "soil"},
		{"ebiomes:chestnut_sapling", ebiomes.grow_new_chestnut_tree, "soil"},
		{"ebiomes:hornbeam_sapling", ebiomes.grow_new_hornbeam_tree, "soil"},
		{"ebiomes:hardy_bush_sapling", ebiomes.grow_new_hardy_bush, "soil"},
		{"ebiomes:thorn_bush_sapling", ebiomes.grow_new_thorn_bush, "soil"},
		{"ebiomes:cowberry_bush_sapling", ebiomes.grow_new_cowberry, "soil"},
		{"ebiomes:afzelia_sapling", ebiomes.grow_new_afzelia_tree, "soil"},
		{"ebiomes:limba_sapling", ebiomes.grow_new_limba_tree, "soil"},
		{"ebiomes:siri_sapling", ebiomes.grow_new_siri_tree, "soil"},
		{"ebiomes:tamarind_sapling", ebiomes.grow_new_tamarind_tree, "soil"},
		{"ebiomes:peashrub_sapling", ebiomes.grow_new_peashrub, "soil"},
		{"ebiomes:bamboo_sprout", ebiomes.grow_new_bamboo, "soil"},
		{"ebiomes:sugi_sapling", ebiomes.grow_new_sugi_tree, "soil"},
		{"ebiomes:mizunara_sapling", ebiomes.grow_new_mizunara_tree, "soil"},
		{"ebiomes:stoneoak_sapling", ebiomes.grow_new_stoneoak_tree, "soil"},
		{"ebiomes:oak_sapling", ebiomes.grow_new_oak_tree, "soil"},
		{"ebiomes:maple_sapling", ebiomes.grow_new_maple_tree, "soil"},
	})

	bonemeal:add_deco({
		{"ebiomes:dirt_with_forest_litter", {"default:grass_1", "default:grass_2", "default:grass_3", "default:grass_4", "default:grass_5", "default:fern_1", "default:fern_2", "default:fern_3"},
			{"flowers:rose"} },
		{"ebiomes:dirt_with_grass_cold", {"ebiomes:grass_cold_1", "ebiomes:grass_cold_2", "ebiomes:grass_cold_3", "ebiomes:grass_cold_4", "ebiomes:grass_cold_5"},
			{"flowers:dandelion_yellow", "flowers:dandelion_white", "flowers:tulip", "flowers:tulip_black", "flowers:chrysanthemum_green"} },
		{"ebiomes:dirt_with_grass_swamp", {"ebiomes:grass_swamp_1", "ebiomes:grass_swamp_2", "ebiomes:grass_swamp_3", "ebiomes:grass_swamp_4", "ebiomes:grass_swamp_5"},
			{"ebiomes:marsh_stitchwort", "ebiomes:marsh_grass_yellow", "ebiomes:marsh_grass_green"} },
		{"ebiomes:peat_with_swamp_moss_yellow", {"ebiomes:grass_bog_1", "ebiomes:grass_bog_2", "ebiomes:grass_bog_3", "ebiomes:grass_bog_4", "ebiomes:grass_bog_5", "ebiomes:reeds"},
			{"ebiomes:cranberry_patch", "ebiomes:sundew", "ebiomes:marigold", "ebiomes:marsh_stitchwort", "ebiomes:marsh_grass_yellow", "ebiomes:marsh_grass_green"} },

		{"ebiomes:peat_wet_with_swamp_moss_green", {"ebiomes:reeds"},
			{"ebiomes:cranberry_patch", "ebiomes:sundew", "ebiomes:marigold"} },
		{"ebiomes:dirt_with_grass_warm", {"ebiomes:grass_warm_1", "ebiomes:grass_warm_2", "ebiomes:grass_warm_3", "ebiomes:grass_warm_4", "ebiomes:grass_warm_5"},
			{"flowers:dandelion_yellow", "flowers:dandelion_white", "flowers:tulip", "flowers:chrysanthemum_green", "ebiomes:larkspur"} },
		{"ebiomes:dry_dirt_with_grass_arid", {"ebiomes:grass_arid_1", "ebiomes:grass_arid_2", "ebiomes:grass_arid_3", "ebiomes:grass_arid_4", "ebiomes:grass_arid_5"},
			{"default:dry_shrub"} },
		{"ebiomes:dry_dirt_with_grass_arid_cool", {"ebiomes:grass_arid_cool_1", "ebiomes:grass_arid_cool_2", "ebiomes:grass_arid_cool_3", "ebiomes:grass_arid_cool_4", "ebiomes:grass_arid_cool_5"},
			{"default:dry_shrub"} },
		{"ebiomes:dirt_with_jungle_savanna_grass", {"ebiomes:jungle_savanna_grass_1", "ebiomes:jungle_savanna_grass_2", "ebiomes:jungle_savanna_grass_3", "ebiomes:jungle_savanna_grass_4", "ebiomes:jungle_savanna_grass_5"},
			{"default:junglegrass"} },
		{"ebiomes:dry_dirt_with_humid_savanna_grass", {"ebiomes:humid_savanna_grass_1", "ebiomes:humid_savanna_grass_2", "ebiomes:humid_savanna_grass_3", "ebiomes:humid_savanna_grass_4", "ebiomes:humid_savanna_grass_5"},
			{"ebiomes:jaragua_grass"} },
		{"ebiomes:dirt_with_grass_steppe", {"ebiomes:grass_steppe_1", "ebiomes:grass_steppe_2", "ebiomes:grass_steppe_3", "ebiomes:grass_steppe_4", "ebiomes:grass_steppe_5"},
			{"ebiomes:altai_tulip", "ebiomes:blue_allium", "ebiomes:blue_allium_purple"} },
		{"ebiomes:dirt_with_grass_steppe_warm", {"ebiomes:grass_steppe_warm_1", "ebiomes:grass_steppe_warm_2", "ebiomes:grass_steppe_warm_3", "ebiomes:grass_steppe_warm_4", "ebiomes:grass_steppe_warm_5"},
			{"ebiomes:russian_iris", "flowers:dandelion_yellow", "flowers:dandelion_white"} },
		{"ebiomes:dirt_with_grass_steppe_cold", {"ebiomes:grass_steppe_cold_1", "ebiomes:grass_steppe_cold_2", "ebiomes:grass_steppe_cold_3", "ebiomes:grass_steppe_cold_4", "ebiomes:grass_steppe_cold_5"},
			{"ebiomes:mountain_lily", "ebiomes:siberian_lily"} },
		{"ebiomes:dirt_with_japanese_rainforest_litter", {"default:fern_1", "default:fern_2", "default:fern_3"},
			{"ebiomes:forestgrowth", "ebiomes:moss"} },
	})

	if minetest.settings:get_bool("reg_warms", true) then
	bonemeal:add_deco({
		{"ebiomes:dirt_with_grass_med", {"ebiomes:grass_med_1", "ebiomes:grass_med_2", "ebiomes:grass_med_3", "ebiomes:grass_med_4", "ebiomes:grass_med_5"},
			{"ebiomes:black_iris", "ebiomes:cladanthus", "ebiomes:savory", "ebiomes:staehelina", "ebiomes:larkspur", "ebiomes:buttercup_persian", "ebiomes:chrysanthemum_yellow"} }
	})
	else
	bonemeal:add_deco({
		{"ebiomes:dirt_with_grass_med", {"ebiomes:grass_med_1", "ebiomes:grass_med_2", "ebiomes:grass_med_3", "ebiomes:grass_med_4", "ebiomes:grass_med_5"},
			{"ebiomes:black_iris", "ebiomes:cladanthus", "ebiomes:savory", "ebiomes:staehelina", "ebiomes:buttercup_persian", "ebiomes:chrysanthemum_yellow"} }
	})
	end

	minetest.register_craft({
--		type = "shapeless",
		output = "bonemeal:mulch 9",
		recipe = {
			{"ebiomes:peat"},
		}
	})
end

